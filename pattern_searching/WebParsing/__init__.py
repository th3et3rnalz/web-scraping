import os, requests, re
from bs4 import BeautifulSoup


def manage(url, name, dirname=None):
    if dirname == None:
        path = 'cached/'+name+'.page'
        if not os.path.exists(path):
            response = requests.get(url).content
            with open(path, 'wb') as f:
                f.write(response)
            f.close()
        response = open(path, 'rb').read()
        return response
    else:
        path = 'cached/' + dirname + "/" + name+'.page'
        if not os.path.exists(path):
            if not os.path.isdir('cached'):
                os.mkdir('chached')
            if not os.path.isdir('cached/'+dirname):
                os.mkdir('cached/'+ dirname)
            response = requests.get(url).content
            with open(path, 'wb') as f:
                f.write(response)
            f.close()
        response = open(path, 'rb').read()
        return response

class ParseWebsite:
    def __init__(self, base_url):
        print(base_url)
        self.base_url = "https://" + base_url.replace("https://", "").split("/")[0]
        self.name = base_url.split("/")[2].replace(".", "")
        self.main_page = BeautifulSoup(manage(base_url, self.name, dirname=self.name), 'html.parser')
        self.all_links = []
        for link in self.main_page.find_all('a'):
            try:
                self.all_links.append([link['href'], link.get_text()])
            except:
                pass
        self.viable_links = []
    
    def get_viable_links(self):
        of_interest = ['home', 'company', 'about us', 'contact']
        
        possible_links = []
        for link in self.all_links:
            for word in of_interest:
                if link[1].lower().find(word) is not -1:
                    if link[0].lower().find(self.base_url) is not -1:
                        possible_links.append(link)
        
        # Be aware, one link may be in there multiple times. Let's fix that:
        for link in possible_links:
            if link[0] not in self.viable_links:
                self.viable_links.append(link[0])
        
        print(self.viable_links)
        return self.viable_links

    def search_all(self):
        if self.viable_links == []:
            self.get_viable_links()
        
        print(self.main_page.find_all(text=re.compile('Contacts')))
        # print(self.main_page.find_all('h1'))
        # print(self.main_page.body.prettify())
        
            


website = ParseWebsite(open('../links.txt').readlines()[0])
# website.get_viable_links()
website.search_all()




class Information:
    def __init__(self):
        self.data = {}

    def add(self, type, info):
        if type not in self.data:
            self.data[type] = [info]
        else:
            self.data[type].append(info)
    
    def print_all(self):
        for key in self.data.keys():
            print("{} :".format(key))
            for el in self.data[key]:
                print('  - {}'.format(el))