from bs4 import BeautifulSoup
from WebParsing import Information, manage


with open('links.txt', 'r') as f:
    links = f.readlines()

soup = BeautifulSoup(manage(links[0], 'leaf'), 'html.parser')


link1 = Information()

# getting the logo
possible_logo = soup.find_all('img')
for p_logo in possible_logo:
    for cls in p_logo['class']:
        if cls.find('logo') is not -1:
            link1.add('logo', p_logo['src'])


# getting the full name
possible_name = soup.find('title')
link1.add('name', possible_name.get_text())

link1.print_all()