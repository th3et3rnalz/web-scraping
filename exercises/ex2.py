# import requests
from bs4 import BeautifulSoup


# response = requests.get("http://catalog.data.gov/dataset?q=&sort=metadata_created+desc")
# file = open('response.content', 'wb')
# file.write(response.content)
# file.close()


file = open('response.content', 'rb')
response = file.read()

soup = BeautifulSoup(response, 'html.parser')
newest_db = soup.find_all('li', class_='dataset-item')[0]
print(newest_db.find_all('h3', class_='dataset-heading')[0].find_all('a')[0].get_text())