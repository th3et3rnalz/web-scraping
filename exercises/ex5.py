import requests
from bs4 import BeautifulSoup
import os
import csv
from shutil import unpack_archive
from statistics import median


response = requests.get('https://publicpay.ca.gov/Reports/RawExport.aspx').content
soup = BeautifulSoup(response, 'html.parser').find('section', class_='download_file_area')

elements = []
for a in soup.find_all('a', href=True):
    for year in ['2010', '2013']:
        if a['href'].find(year) is not -1:
            if a.get_text().find('City') is not -1:
                elements.append(a)

a = "https://publicpay.ca.gov/RawExport/2016_County.zip"
base_url = "https://publicpay.ca.gov"

for a in elements:
    url = base_url + a['href']
    localname = "/tmp/" + a['href'].replace("/RawExport/", "")
    if not os.path.exists(localname):
        data = requests.get(url).content
        with open(localname, 'wb') as f:
            f.write(data)
    unpack_archive(localname, "/tmp/scraping", format='zip')
    csv_name = "/tmp/scraping" + "/" + a['href'].replace("/RawExport/", "").replace("zip", "csv")
    with open(csv_name, 'r', encoding='latin-1') as f:
        cx = list(csv.DictReader(f.readlines()[4:]))
        # print(cx)
        # mx = median([float(row['Health Dental Vision']) for row in cx if row['Health Dental Vision']])
        # print("Median for {} is {}%".format(a['href'].replace("/RawExport/", ""), mx))


